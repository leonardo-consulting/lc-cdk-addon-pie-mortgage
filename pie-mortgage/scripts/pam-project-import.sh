#!/bin/sh

SLEEPTO=30
BUSINESS_CENTRAL_HOSTNAME_HTTPS=https://pie-rhpam74-rhpamcentr.dev-sandbox.svc.cluster.local:8443 
KIE_SERVER_HOSTNAME_HTTPS=https://pie-rhpam74-rhpamcentr.dev-sandbox.svc.cluster.local:8443 
KIE_ADMIN_USER=kieAdmin
KIE_ADMIN_PWD=kieAdmin!23
KIE_SERVER_ID=pie-rhpam74-kieserver
SPACE_NAME=Pamlets
PROJECT_NAME=Mortgage_Process
GIT_URL=https://bitbucket.org/leonardo-consulting/pamlet-mortgage-pam.git 
GROUP_ID=mortgage-process
ARTIFACT_ID=mortgage-process
VERSION=1.0.0-SNAPSHOT
CONTAINER_ID=${ARTIFACT_ID}_${VERSION}
#Optional Variables (needed since this is currently hosted in a private repo)
#GIT_USER= Currently not required due to making repo public during testing.
#GIT_PASSWORD=
KSESSION=
KBASE=
RUNTIME_STRATEGY=SINGLETON
#RUNTIME_STRATEGY (defaults to "SINGLETON")

check() {
proj_inserted=$(curl -s -k --request GET \
--url ${BUSINESS_CENTRAL_HOSTNAME_HTTPS}/rest/controller/management/servers/pie-rhpam74-kieserver/containers \
--user "${KIE_ADMIN_USER}:${KIE_ADMIN_PWD}" | grep "<artifact-id>${ARTIFACT_ID}</artifact-id>")
if [ ! -z "${proj_inserted}" ]; then
   echo "${proj_inserted} already exist in BC...not re-inserting..."
   exit 0
fi

}

curl1() {
echo -e "\n==========================[EXECUTING] curl1 =========================="
curlone_job_id=$(curl -s -k --request POST \
--url ${BUSINESS_CENTRAL_HOSTNAME_HTTPS}/rest/spaces/ \
--user "${KIE_ADMIN_USER}:${KIE_ADMIN_PWD}" \
--header 'content-type: application/json' \
--data "{
\"name\": \"${SPACE_NAME}\",
\"description\": \"A space for pamlet imports.\",
\"owner\": \"${KIE_ADMIN_USER}\",
\"defaultGroupId\": \"com.${SPACE_NAME}\"
}" | awk -F\, '{print $1}' | awk -F\: '{print $2}' | sed 's/"//g')
sleep 2

sleepincrone=0
while [ "${curlone_job_id_state}" != "SUCCESS" ]
do
        curlone_job_id_state=$(curl -s -k --request GET \
--url ${BUSINESS_CENTRAL_HOSTNAME_HTTPS}/rest/jobs/${curlone_job_id} \
--user "${KIE_ADMIN_USER}:${KIE_ADMIN_PWD}" | awk -F\, '{print $1}' | awk -F\: '{print $2}' | sed 's/"//g' )

        echo "JOBID=${curlone_job_id} JOBIDSTATE=${curlone_job_id_state}"
        echo "sleep 1"; sleep 1
        sleepincrone=$(expr ${sleepincrone} + 1)
        case "${sleepincrone}" in
                ${SLEEPTO}) return
        esac
done

}

curl2() {

echo -e "\n==========================[EXECUTING] curl2 =========================="
curltwo_job_id=$(curl -s -k --request POST \
--url ${BUSINESS_CENTRAL_HOSTNAME_HTTPS}/rest/spaces/${SPACE_NAME}/git/clone \
--user "${KIE_ADMIN_USER}:${KIE_ADMIN_PWD}" \
--header 'content-type: application/json' \
--data "{
\"name\": \"${PROJECT_NAME}\",
\"description\": \"[Optional] Description\",
\"userName\": \"${GIT_USER}\",
\"password\": \"${GIT_PASSWORD}\",
\"gitURL\": \"${GIT_URL}\"
}" | awk -F\, '{print $1}' | awk -F\: '{print $2}' | sed 's/"//g')

sleepincrtwo=0
while [ "${curltwo_job_id_state}" != "SUCCESS" ]
do
        curltwo_job_id_state=$(curl -s -k --request GET \
--url ${BUSINESS_CENTRAL_HOSTNAME_HTTPS}/rest/jobs/${curltwo_job_id} \
--user "${KIE_ADMIN_USER}:${KIE_ADMIN_PWD}" | awk -F\, '{print $1}' | awk -F\: '{print $2}' | sed 's/"//g' )

        echo "JOBID=${curltwo_job_id} JOBIDSTATE=${curltwo_job_id_state}"
        echo "sleep 1"; sleep 1
        sleepincrtwo=$(expr ${sleepincrtwo} + 1)
        case "${sleepincrtwo}" in
                ${SLEEPTO}) return
        esac
done

}

curl3() {

echo -e "\n==========================[EXECUTING] curl3 =========================="
curlthree_job_id=$(curl -s -k --request POST \
--url ${BUSINESS_CENTRAL_HOSTNAME_HTTPS}/rest/spaces/${SPACE_NAME}/projects/${PROJECT_NAME}/maven/deploy \
--user "${KIE_ADMIN_USER}:${KIE_ADMIN_PWD}" | awk -F\, '{print $1}' | awk -F\: '{print $2}' | sed 's/"//g')
sleep 2

sleepincrthree=0
while [ "${curlthree_job_id_state}" != "SUCCESS" ]
do
        curlthree_job_id_state=$(curl -s -k --request GET \
--url ${BUSINESS_CENTRAL_HOSTNAME_HTTPS}/rest/jobs/${curlthree_job_id} \
--user "${KIE_ADMIN_USER}:${KIE_ADMIN_PWD}" | awk -F\, '{print $1}' | awk -F\: '{print $2}' | sed 's/"//g' )

        echo "JOBID=${curlthree_job_id} JOBIDSTATE=${curlthree_job_id_state}"
        echo "sleep 1"; sleep 1
        sleepincrthree=$(expr ${sleepincrthree} + 1)
        case "${sleepincrthree}" in
                ${SLEEPTO}) return
        esac
done

}

curl4() {
echo -e "\n==========================[EXECUTING] curl4 =========================="
curlfour_job_id=$(curl -s -k --request PUT \
--url ${BUSINESS_CENTRAL_HOSTNAME_HTTPS}/rest/controller/management/servers/${KIE_SERVER_ID}/containers/${CONTAINER_ID} \
--user "${KIE_ADMIN_USER}:${KIE_ADMIN_PWD}" \
--header 'content-type: application/json' \
--data "{
\"container-id\" : \"${CONTAINER_ID}\",
\"container-name\" : \"${ARTIFACT_ID}\",
\"server-template-key\" : {
\"server-id\": \"${KIE_SERVER_ID}\",
\"server-name\": \"${KIE_SERVER_ID}\"
},
\"release-id\" : {
\"group-id\" : \"${GROUP_ID}\",
\"artifact-id\" : \"${ARTIFACT_ID}\",
\"version\" : \"${VERSION}\"
},
\"configuration\" : {
\"RULE\" : {
\"org.kie.server.controller.api.model.spec.RuleConfig\" : {
\"pollInterval\" : null,
\"scannerStatus\" : \"STOPPED\"
}
},
\"PROCESS\" : {
\"org.kie.server.controller.api.model.spec.ProcessConfig\" : {
\"runtimeStrategy\" : \"${RUNTIME_STRATEGY}\",
\"kbase\" : \"${KBASE}\",
\"ksession\" : \"${KSESSION}\",
\"mergeMode\" : \"MERGE_COLLECTIONS\"
}
}
},
\"status\" : \"STARTED\"
}"| awk -F\, '{print $1}' | awk -F\: '{print $2}' | sed 's/"//g')

sleepincrfour=0
while [ "${curlfour_job_id_state}" != "SUCCESS" ]
do
        curlfour_job_id_state=$(curl -s -k --request GET \
--url ${BUSINESS_CENTRAL_HOSTNAME_HTTPS}/rest/jobs/${curlfour_job_id} \
--user "${KIE_ADMIN_USER}:${KIE_ADMIN_PWD}" | awk -F\, '{print $1}' | awk -F\: '{print $2}' | sed 's/"//g' )

        echo "JOBID=${curlfour_job_id} JOBIDSTATE=${curlfour_job_id_state}"
        echo "sleep 1"; sleep 1
        sleepincrfour=$(expr ${sleepincrfour} + 1)
        case "${sleepincrfour}" in
                ${SLEEPTO}) return
        esac
done

}

check
curl1
curl2
curl3
curl4

